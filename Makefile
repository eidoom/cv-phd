DOCNAME=cv
TEX=xelatex

all: report

.PHONY: clean

report:
	$(TEX) $(DOCNAME).tex

view: report
	evince $(DOCNAME).pdf &

clean:
	rm *.blg *.bbl *.aux *.log *.out *.toc

